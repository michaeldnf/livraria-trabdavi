<?php

class Conexao {
    # Variável que guarda a conexão PDO.

    private $db;
    private $db_sem_bd;
    private $erro = "sucesso";

    # Private construct - garante que a classe só possa ser instanciada internamente.

    public function __construct() {

        # Informações sobre o banco de dados:
        $db_host = "localhost";
        $db_nome = "livraria";
        $db_usuario = "root";
        $db_senha = "";
        $db_driver = "mysql";

        try {
            # Atribui o objeto PDO à variável $db.
            $this->db = new PDO("$db_driver:host=$db_host; dbname=$db_nome", $db_usuario, $db_senha);
            # Garante que o PDO lance exceções durante erros.
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            # Garante que os dados sejam armazenados com codificação UFT-8.
            $this->db->exec('SET NAMES utf8');
        } catch (PDOException $e) {
            # Envia um e-mail para o e-mail oficial do sistema, em caso de erro de conexão.
            //   mail($sistema_email, "PDOException em $sistema_titulo", $e->getMessage());
            # Então não carrega nada mais da página.
            $this->erro = "Erro não tratado: " . $e->getMessage();
        }
    }

    # Método estático - acessível sem instanciação.

    public  function Conectar() {
        # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if (!$this->db) {
            new Conexao();
        }
        # Retorna a conexão.
        return $this->db;
    }

    # Método estático - acessível sem instanciação.

    public  function v_erro() {
      //  $this->Conexao();
        return $this->erro;
    }

}

//$pdo = New Conexao();

//var_dump($pdo->v_erro());
?>
