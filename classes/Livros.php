<?php

class Livros extends Conexao {

    public function SelecionaLivros() {
        $sql = "SELECT * FROM livro";
        $sql = $this->Conectar()->query($sql);
        return $sql;
    }

    public function SelecionaLivroId($id) {
        $sql = "SELECT * FROM livro where id = $id";
        $sql = $this->Conectar()->query($sql);
        return $sql;
    }
    public function InserirLivro($titulo, $descr, $vr, $autor, $img) {
        try {
            $sql = "INSERT INTO livro (titulo, descr, vr, autor, img) VALUES (:titulo, :descr, :vr, :autor, :img )";

            $sql = $this->Conectar()->prepare($sql);
            $sql->bindValue(':titulo', $titulo);
            $sql->bindValue(':descr', $descr);
            $sql->bindValue(':vr', $vr);
            $sql->bindValue(':autor', $autor);
            $sql->bindValue(':img', $img);
            $sql->execute();
        } catch (PDOException $e) {
            Echo "Deu erro";
            echo $e->getMessage();
        }
    }

    public function AtualizarLivros($id, $titulo, $descr, $vr, $autor) {
        try {
            $sql = "UPDATE  livro SET titulo = :titulo, descr = :descr, vr = :vr, autor = :autor WHERE id = :id";

            $sql = $this->Conectar()->prepare($sql);
            $sql->bindValue(':titulo', $titulo);
            $sql->bindValue(':descr', $descr);
            $sql->bindValue(':vr', $vr);
            $sql->bindValue(':autor', $autor);
            $sql->bindValue(':id', $id);
            $sql->execute();
        } catch (PDOException $e) {
            Echo "Deu erro";
            echo $e->getMessage();
        }
    }

    public function ExcluirLivros($id) {
        try {
            $sql = "DELETE FROM livro WHERE id = :id";

            $sql = $this->Conectar()->prepare($sql);
            $sql->bindValue(':id', $id);
            $sql->execute();
        } catch (PDOException $e) {
            Echo "Deu erro";
            echo $e->getMessage();
        }
    }

}
