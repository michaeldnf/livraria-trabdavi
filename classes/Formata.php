<?php

class Formata {

    public static function Real($valor) {
        return number_format($valor, 2, ',', '.');
    }

    public static function Float($valor) {
        $verificaPonto = ".";
        if (strpos("[" . $valor . "]", "$verificaPonto")):
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
        else:
            $valor = str_replace(',', '.', $valor);
        endif;

        return $valor;
    }

    public static function Positivo($valor) {
        if ($valor < 0) {
            return $valor * - 1;
        } else {
            return $valor;
        }
    }

}

?>