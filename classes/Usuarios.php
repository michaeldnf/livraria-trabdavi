<?php

class Usuarios extends Conexao {

    public function Logar($login, $senha) {
        $sql = "SELECT * FROM usuario WHERE email = :login AND senha = :senha";
        $sql = $this->Conectar()->prepare($sql);
        $sql->bindValue(":login", $login);
        $sql->bindValue(":senha", $senha);
        $sql->execute();
        $resutados = $sql->fetchAll(PDO::FETCH_ASSOC);
        if ($sql->rowCount() == 1) {
            foreach ($resutados as $key) {
                $_SESSION['nmUser'] = $key['nm'];
            }

            echo true;
        } else {
            echo "Login ou senha incorretos";
        }
    }
    
    public function Logado() {
         if(ISSET($_SESSION['nmUser']) && $_SESSION['nmUser'] <> null){
             
             return TRUE;
         }
         return FALSE;
    }
    
    public function sair() {
       $_SESSION['nmUser'] = null;
    }

}
