<?php
    require_once __DIR__.'/autoLoad.php';
    $livros = new Livros();
    $livros->SelecionaLivros();

    $usuario = new Usuarios();
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Meta tags Obrigatórias -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="icon" href="img/bookmark.png">
        <title>Livraria - MHSystem</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand text-primary" href="index.php"><img style="width: 50px;" src="img/bookmark.png"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0 ml-sm-2">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Consulta</a>
                    </li>
                    <li class="nav-item">
                        <?php
                        if(!$usuario->Logado()){
                            echo ' <a class="nav-link" href="login.html">Login</a>';
                            
                        }else{
                             echo ' <a class="nav-link" href="index.php?nav=administrativo">Painel Administrativo</a>';
                             echo ' <a class="nav-link" href="php/Sair.php">Sair</a>';
                        }
                        
                        ?>
                       
                    </li>
                </ul>
            </div>
        </nav>

        <!-- Modal -->
        <div class="modal fade" id="modalProduto">
          <div class="modal-dialog" >
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Título do modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                  <span>&times;</span>
                </button>
              </div>
              <div class="modal-body">
                ...
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="confirmaCompra()">CONFIRMAR COMPRA</button>
              </div>
            </div>
          </div>
        </div>
        <?php
            $nav = "";
            if(ISSET($_GET['nav'])){
              $nav  = $_GET['nav'];
            }else{
                 require_once './home.php';
            }
            switch ($nav)  {
                case "administrativo":
                    if(!$usuario->Logado()){
                        header("Location: login.html");
                    }
                    require_once 'admin/painelcontrole.php';
                    break;
                  
            }
        ?>
       
        

        <!-- JavaScript (Opcional) -->
        <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
        <script src="js/jquery.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/script.js"></script>
    </body>
</html>