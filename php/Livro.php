<?php

require_once '../autoLoad.php';
$livros = new Livros();

$dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);

if ($dados['botao'] == "cadastrar") {
    $image = new Image("../img", "images");
    if ($_FILES) {
        try {
            $caminho = $image->upload($_FILES['img'], $dados['nome']);
        } catch (Exception $e) {
            echo "<p>(!) {$e->getMessage()}</p>";
        }
    }

    $erro = [];
    !isset($dados['nome']) ? array_push($erro, "Faltou o nome") : 0;
    !isset($dados['descricao']) ? array_push($erro, "Faltou a descrição") : 0;
    !isset($dados['preco']) ? array_push($erro, "Faltou o preço") : 0;
    !isset($dados['autor']) ? array_push($erro, "Faltou o autor") : 0;
    !isset($_FILES['img']) ? array_push($erro, "Faltou a imagem") : 0;

    if (count($erro) > 0) {
        foreach ($erro as $key) {
            echo $key . "<br>";
        }
        exit();
    }

    $livros->InserirLivro($dados['nome'], $dados['descricao'], $F::Float($dados['preco']), $dados['autor'], $caminho);
    header("Location: ../index.php?nav=administrativo");
}

if ($dados['botao'] == "atualizar") {
    $erro = [];
    !isset($dados['nome']) ? array_push($erro, "Faltou o nome") : 0;
    !isset($dados['descricao']) ? array_push($erro, "Faltou a descrição") : 0;
    !isset($dados['preco']) ? array_push($erro, "Faltou o preço") : 0;
    !isset($dados['autor']) ? array_push($erro, "Faltou o autor") : 0;

    if (count($erro) > 0) {
        foreach ($erro as $key) {
            echo $key . "<br>";
        }
        exit();
    }

    $livros->AtualizarLivros($dados['id'], $dados['nome'], $dados['descricao'], $F::Float($dados['preco']), $dados['autor']);
    header("Location: ../index.php?nav=administrativo");
}


if ($dados['botao'] == "excluir") {

    $livros->ExcluirLivros($dados['id']);
    header("Location: ../index.php?nav=administrativo");
}


?>