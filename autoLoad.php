<?php

session_start();


spl_autoload_register(
        function ($arquivo) {

    $arquivo = $arquivo . ".php";

    $caminho = "classes/" . $arquivo;
    // echo $caminho."<br>".__DIR__."<br>";
    if (file_exists($caminho)) {
        include_once $caminho;
    } else if (file_exists("../" . $caminho)) {
        include_once '../' . $caminho;
    } else if (file_exists("../../" . $caminho)) {
        include_once '../../' . $caminho;
    } else if (file_exists("../" . $arquivo)) {
        include_once '../' . $arquivo;
    } else if (file_exists("../../" . $arquivo)) {
        include_once '../../' . $arquivo;
    } else if (file_exists($arquivo)) {
        include_once $arquivo;
    } else {
        echo 'Não encontrou';
        echo "<br>" . $arquivo;
    }
});


$F = new Formata();
?>