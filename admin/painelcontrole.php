<link rel="stylesheet" href="admin/bootstrap.css">
<div class="topocadastro"> <h4>Cadastro de Livro</h4></div>

<div class="cadastrar">
    <form action="php/Livro.php" method="POST" enctype="multipart/form-data">

        <div class="nomecadastro">  <h5>Nome do Livro</h5>
            <input type="text" name="nome"  class="form-control" 
                   placeholder="Nome do livro">  </div>
        <div class="foto"> <h5>Imagem do Livro</h5> 
            <input type="file" class="form-control-file" name="img" >
        </div>
        <div class="autor">  <h5>Autor do Livro</h5> 
            <input type="text" name="autor"    class="form-control" placeholder="Nome do Autor"> </div>
        <div class="preco">  <h5>Preço do Livro</h5>
            <input type="text" name="preco"   class="form-control" placeholder="Preço"> </div>
        <div class="descricao">  <h5>Descrição do Livro</h5>
            <textarea class="descricaotexto" name="descricao"></textarea>
        </div>

        <button name="botao" value="cadastrar" 
                type="submit" class="btn btn-primary" value="cadastrar" id="cadastrar" >Cadastrar</button>
    </form>
</div>
<div class="topocadastro"> <h4>Auteração de Livro</h4></div>
<div class="atualizar">
    <?php
    $livros = new Livros();

    foreach ($livros->SelecionaLivros() as $key) {
        ?>
        <form name="atualizar" action="php/Livro.php" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id"  class="form-control" value="<?= $key['id']; ?> "> 
            <div class="nomeatualizar">
                <h5>Nome do Livro </h5>
                <input type="text" name="nome"  class="form-control" value="<?= $key['titulo']; ?> " placeholder="Nome do livro"> 
            </div>

            <div class="imgatualizar">
                <h5>Imagem do Livro </h5>
                <img src="php/<?= $key['img'] ?>" width="100%" height="100px"/>
            </div>

            <div class="autoratualizar">
                <h5>Nome do Autor </h5>
                <input type="text" name="autor"  class="form-control" value="<?= $key['autor']; ?> " placeholder="Nome do livro"> 
            </div>
            <div class="precoatualizar">
                <h5>Preço do Livro </h5>
                <input type="text" name="preco" step="0.01"  class="form-control" value="<?= $F::Real($key['vr']); ?> " placeholder="Valor do livro"> 
            </div>
            <div class="descricao">  <h5>Descrição do Livro</h5>
                <textarea class="descricaotexto" name="descricao" ><?= $key['descr']; ?></textarea>
            </div>

            <div class="botoes">
                <button name="botao" value="atualizar"  type="submit" class="btn btn-atualiza" name="botao" name="atualizar" >Atualizar</button>
                <button name="botao" value="excluir"  type="submit" class="btn btn-exclui" name="botao" name="excluir" >Excluir</button>
            </div>
        </form>
        <?php
    }
    ?>
</div>


















